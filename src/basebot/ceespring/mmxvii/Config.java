/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basebot.ceespring.mmxvii;

import java.io.IOException;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;
import org.wikipedia.BaseBot;

/**
 *
 * @author Base <base-w@yandex.ru>
 */
public class Config {

    static Calendar contestStart = new GregorianCalendar(2017, 2, 20,
            24, 00, 00);
    static Calendar contestEnd = new GregorianCalendar(2017, 4, 31,
            23, 59, 59);

    public static Map<String, Map<String, String>> populateTopics(BaseBot d) throws IOException {
        final Map<String, Map<String, String>> topics = new HashMap<>();
//        topics.put("Culture", d.getSiteLinks("Q23536919"));
//        topics.put("Earth", d.getSiteLinks("Q23531225"));
//        topics.put("Economics", d.getSiteLinks("Q23470319"));
//        topics.put("Society", d.getSiteLinks("Q23541379"));
//        topics.put("Sports", d.getSiteLinks("Q23640309"));
//        topics.put("Politics", d.getSiteLinks("Q23541381"));
//        topics.put("Transport", d.getSiteLinks("Q23640610"));
//        topics.put("History", d.getSiteLinks("Q23640619"));
//        topics.put("Science", d.getSiteLinks("Q23541376"));
//        topics.put("Education", d.getSiteLinks("Q23640622"));
//        topics.put("Women", d.getSiteLinks("Q23640631"));
        return topics;
    }

    public static Map<String, Map<String, String>> populateCountries(BaseBot d) throws IOException {
        final Map<String, Map<String, String>> countries = new HashMap<>();
//        countries.put("Albania", d.getSiteLinks("Q23728001"));
//        countries.put("Armenia", d.getSiteLinks("Q23649574"));
//        countries.put("Austria", d.getSiteLinks("Q23437429"));
//        countries.put("Azerbaijan", d.getSiteLinks("Q23649562"));
//        countries.put("Republic of Bashkortostan", d.getSiteLinks("Q23728012"));
//        countries.put("Belarus", d.getSiteLinks("Q23647470"));
//        countries.put("Bosnia and Herzegovina", d.getSiteLinks("Q23714165"));
//        countries.put("Bulgaria", d.getSiteLinks("Q23649555"));
//        countries.put("Croatia", d.getSiteLinks("Q23649747"));
//        countries.put("Czech Republic", d.getSiteLinks("Q23649578"));
//        countries.put("Esperantujo", d.getSiteLinks("Q23649748"));
//        countries.put("Estonia", d.getSiteLinks("Q23648998"));
//        countries.put("Georgia", d.getSiteLinks("Q23649750"));
//        countries.put("Greece", d.getSiteLinks("Q23649563"));
//        countries.put("Hungary", d.getSiteLinks("Q23437308"));
//        countries.put("Kazakhstan", d.getSiteLinks("Q23714164"));
//        countries.put("Kosovo", d.getSiteLinks("Q23712690"));
//        countries.put("Latvia", d.getSiteLinks("Q23647476"));
//        countries.put("Lithuania", d.getSiteLinks("Q23647474"));
//        countries.put("Macedonia", d.getSiteLinks("Q23649564"));
//        countries.put("Moldova", d.getSiteLinks("Q23649554"));
//        countries.put("Poland", d.getSiteLinks("Q23649565"));
//        countries.put("Romania", d.getSiteLinks("Q23647481"));
//        countries.put("Russia", d.getSiteLinks("Q23647479"));
//        countries.put("Sakha Republic", d.getSiteLinks("Q23714163"));
//        countries.put("Republika Srpska", d.getSiteLinks("Q23649577"));
//        countries.put("Serbia", d.getSiteLinks("Q23538283"));
//        countries.put("Slovakia", d.getSiteLinks("Q23437381"));
//        countries.put("Turkey", d.getSiteLinks("Q23649760"));
//        countries.put("Ukraine", d.getSiteLinks("Q23498134"));
        return countries;
    }

    /**
     * Provides hardcoded information about the wikis timezone, DST usage and
     * regex string for parsing talk page template for given wiki
     *
     * @todo all the hardcoded information should be read from an on-wiki config
     * instead
     * @todo remove duplicate switch statement
     */
    public static class Wiki {

        String wikiName;
        int timeZoneOffset;
        boolean usesDST;
        String templateName;
        String templateRegex;
        String[] talkPages;

        Wiki(String wikiName) {
            this.wikiName = wikiName;
            switch (wikiName) {
                //UTC+5
                case "ba.wikipedia.org":
                    timeZoneOffset = 5;
                    usesDST = false;
                    break;

                //UTC+4
                case "az.wikipedia.org":
                case "ka.wikipedia.org":
                    timeZoneOffset = 4;
                    usesDST = false;
                    break;

                // FET/UTC+3 no DST    
                case "be.wikipedia.org":
                case "be-tarask.wikipedia.org":
                case "el.wikipedia.org":
                case "ru.wikipedia.org":
                    timeZoneOffset = 3;
                    usesDST = false;
                    break;

                //UTC+2 no DST
                case "bg.wikipedia.org":
                case "hu.wikipedia.org":
                case "ro.wikipedia.org":
                case "tr.wikipedia.org":
                    timeZoneOffset = 2;
                    usesDST = false;
                    break;

                //EET/EEST
                case "lv.wikipedia.org":
                case "uk.wikipedia.org":
                    timeZoneOffset = 2;
                    usesDST = true;
                    break;
                //CET/CEST
                case "de.wikipedia.org":
                case "mk.wikipedia.org":
                case "sr.wikipedia.org":
                    timeZoneOffset = 1;
                    usesDST = true;
                    break;

                //UTC - intentional or no data
                case "eo.wikipedia.org":
                case "crh.wikipedia.org":
                case "et.wikipedia.org":
                case "lt.wikipedia.org":
                case "myv.wikipedia.org":
                case "pl.wikipedia.org":
                case "sk.wikipedia.org":
                case "sq.wikipedia.org":
                    timeZoneOffset = 0;
                    usesDST = false;
                    break;
                default:
                    timeZoneOffset = 0;
                    usesDST = false;

            }
            templateRegex = getWikiTemplateRegex(wikiName);

        }

    }

    /**
     * Returns regular expression string for parsing talk page template for
     * given wiki. All the values are hardcoded
     *
     * @param wikiName
     * @return
     * @todo Read the data from elsewhere
     */
    public static String getWikiTemplateRegex(String wikiName) {
        String regex = "";
        switch (wikiName) {//perfectly should be moved to some config
            case "az.wikipedia.org":
                regex = "(?s).*\\{\\{\\s*[Vv]ikibahar[ _]2017\\s*\\|[^}]*(istifadəçi)\\s*=([^\\|}]+)[^}]*}}.*";
                break;
            case "ba.wikipedia.org":
                regex = "(?s).*\\{\\{\\s*[Вв]ики-яҙ[ _]2017\\s*\\|[^}]*(ҡатнашыусы)\\s*=([^\\|}]+)[^}]*}}.*";
                break;
            case "be-tarask.wikipedia.org":
                regex = "(?s).*\\{\\{\\s*[Аа]ртыкул[ _]ВікіВясны-2017\\s*\\|[^}]*(удзельнік)\\s*=([^\\|}]+)[^}]*}}.*";
                break;
            case "be.wikipedia.org":
                regex = "(?s).*\\{\\{\\s*[Cc]EE[ _]Spring[ _]2017\\s*\\|[^}]*(удзельнік)\\s*=([^\\|}]+)[^}]*}}.*";
                break;
            case "bg.wikipedia.org":
                regex = "(?s).*\\{\\{\\s*[Cc]EE[ _]Spring[ _]2017\\s*\\|[^}]*(потребител)\\s*=([^\\|}]+)[^}]*}}.*";
                break;
            case "crh.wikipedia.org":
                regex = "(?s).*\\{\\{\\s*[Cc]EE[ _]Spring[ _]2017\\s*\\|[^}]*(qullanıcı)\\s*=([^\\|}]+)[^}]*}}.*";
                break;
            case "de.wikipedia.org":
                regex = "(?s).*\\{\\{\\s*[Cc]EE[ _]Spring[ _]2017\\s*\\|[^}]*(benutzer)\\s*=([^\\|}]+)[^}]*}}.*";
                break;
            case "el.wikipedia.org":
                regex = "(?s).*\\{\\{\\s*[Cc]EE[ _]Spring[ _]2017\\s*\\|[^}]*(user)\\s*=([^\\|}]+)[^}]*}}.*";
                break;
            case "eo.wikipedia.org":
                regex = "(?s).*\\{\\{\\s*[Vv]iki-printempo[ _]2017\\s*\\|[^}]*(user)\\s*=([^\\|}]+)[^}]*}}.*";
                break;
            case "ka.wikipedia.org":
                regex = "(?s).*\\{\\{\\s*ვიკიგაზაფხული[ _]2017\\s*\\|[^}]*(მომხმარებელი)\\s*=([^\\|}]+)[^}]*}}.*";
                break;
            case "lt.wikipedia.org":
                regex = "(?s).*\\{\\{\\s*[Vv]RE[ _]2017\\s*\\|[^}]*(naudotojas)\\s*=([^\\|}]+)[^}]*}}.*";
                break;
            case "lv.wikipedia.org":
                regex = "(?s).*\\{\\{\\s*[Cc]EE[ _]Spring[ _]2017\\s*\\|[^}]*(dalībnieks)\\s*=([^\\|}]+)[^}]*}}.*";
                break;
            case "mk.wikipedia.org":
                regex = "(?s).*\\{\\{\\s*[Сс]ИЕ[ _]Пролет[ _]2017\\s*\\|[^}]*(корисник)\\s*=([^\\|}]+)[^}]*}}.*";
                break;
            case "myv.wikipedia.org":
                regex = "(?s).*\\{\\{\\s*[Вв]икиТундо[ _]2017\\s*\\|[^}]*(сёрмадыця)\\s*=([^\\|}]+)[^}]*}}.*";
                break;
            case "pl.wikipedia.org":
                regex = "(?s).*\\{\\{\\s*[Cc]EE[ _]Spring[ _]2017\\s*\\|[^}]*(autor)\\s*=([^\\|}]+)[^}]*}}.*";
                break;
            case "ro.wikipedia.org":
                regex = "(?s).*\\{\\{\\s*[Ww]ikimedia CEE Spring[ _]2017\\s*\\|[^}]*(utilizator)\\s*=([^\\|}]+)[^}]*}}.*";
                break;
            case "ru.wikipedia.org":
                regex = "(?s).*\\{\\{\\s*[Вв]ики-весна[ _]2017\\s*\\|[^}]*(участник)\\s*=([^\\|}]+)[^}]*}}.*";
                break;
            case "sq.wikipedia.org":
                regex = "(?s).*\\{\\{\\s*[Cc]EE[ _]Spring[ _]2017\\s*\\|[^}]*(user)\\s*=([^\\|}]+)[^}]*}}.*";
                break;
            case "tr.wikipedia.org":
                regex = "(?s).*\\{\\{\\s*[Vv]ikibahar[ _]2017\\s*\\|[^}]*(kullanıcı)\\s*=([^\\|}]+)[^}]*}}.*";
                break;
            case "tt.wikipedia.org":
                regex = "(?s).*\\{\\{\\s*[Вв]ики-яз[ _]2017\\s*\\|[^}]*(кулланучы)\\s*=([^\\|}]+)[^}]*}}.*";
                break;
            case "uk.wikipedia.org":
                regex = "(?s).*\\{\\{\\s*[Cc]EE[ _]Spring[ _]2017\\s*\\|[^}]*(користувач)\\s*=([^\\|}]+)[^}]*}}.*";
                break;
        }
        return regex;
    }

}
