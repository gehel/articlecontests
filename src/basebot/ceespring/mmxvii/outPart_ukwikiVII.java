/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basebot.ceespring.mmxvii;

import basebot.ceespring.out.*;
import basebot.ceespring.CEESMMXVIUAMarkedArticle;
import basebot.ceespring.DatabasePart;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import org.wikipedia.BaseBot;

/**
 *
 * @author Base <base-w at yandex.ru>
 */
public class outPart_ukwikiVII {

    static int WEEK = 1;
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        System.out.println("( 3826 / 1000 ) *   1.8 *  0.8 * 1 + 0 = " + ((3826 / 1000) * 1.8 * 0.8 * 1 + 0));
        System.out.println("( 3826 / 1000 ) *   1.8 *  0.8 * 1 + 0 = "
                + (new BigDecimal(3826).divide(
                        new BigDecimal(1000)).multiply(
                        new BigDecimal(1.8)).multiply(
                        new BigDecimal(0.8)).multiply(
                        new BigDecimal(1)).add(
                        new BigDecimal(0))));

        //<editor-fold  desc="loggin in">
        BaseBot w = new BaseBot("uk.wikipedia.org");
        w.setUserAgent("BaseBot");
        w.login(args[0], args[1]);
        w.setMarkBot(true);
        w.setMarkMinor(true);
        //</editor-fold>

        DatabasePart db = new DatabasePart();
        Connection conn = db.conn;
        Set<CEESMMXVIUAMarkedArticle> articles = new HashSet<>();
        for (int week = 1; week < 12; week++) {
            String page = "Вікіпедія:CEE Spring 2017/Тиждень " + week;
            String[] existing_pages = {page};
            boolean exist = w.exists(existing_pages)[0];
            Map<String, CEESParsedUkwikiListedArticleVII> parsed = new HashMap<>();
            if (exist) {
                String pt = w.getPageText(page);

                String[] arts = pt.split("<!-- стаття з ідентифікатором");
                for (int i = 1; i < arts.length; i++) {
                    String art = arts[i];
                    String id = art.replaceAll("(?s)[^\\d]*([0-9]+).*-->.*", "$1");
                    System.out.println("id = " + id);
                    String isalist = art.replaceAll("(?s).*\\|\\s*є_списком\\s*=(.*)\\|\\s*дискваліфіковано.*", "$1").trim();
                    String disqualified = art.replaceAll("(?s).*\\|\\s*дискваліфіковано\\s*=(.*)\\|\\s*зауваження.*", "$1").trim();
                    String notes = art.replaceAll("(?s).*\\|\\s*зауваження\\s*=(.*)\\|\\s*Mcoffsky.*", "$1").trim();
                    System.out.println("notes = " + notes);
                    String Mcoffsky = art.replaceAll("(?s).*\\|\\s*Mcoffsky\\s*=(.*)\\|\\s*Стефанко1982.*", "$1").trim();
                    System.out.println("Mcoffsky =" + Mcoffsky);
                    String Стефанко1982 = art.replaceAll("(?s).*\\|\\s*Стефанко1982\\s*=([^\\|]*)\\|\\s*Zheliba.*", "$1").trim();
                    System.out.println("Стефанко1982 = " + Стефанко1982);
                    String Zheliba = art.replaceAll("(?s).*\\|\\s*Zheliba\\s*=(.*)\\|\\s*Звірі.*", "$1").trim();
                    System.out.println("Zheliba = " + Zheliba);
                    String Звірі = art.replaceAll("(?s).*\\|\\s*Звірі\\s*=(.*)\\|\\s*Dim_Grits.*", "$1").trim();
                    System.out.println("Звірі = " + Звірі);
                    String Dim_Grits = art.replaceAll("(?s).*\\|\\s*Dim_Grits\\s*=(.*)\\|\\s*підсвітка.*", "$1").trim();
                    System.out.println("Dim_Grits = " + Dim_Grits);
                    String highlight = art.replaceAll("(?s).*\\|\\s*підсвітка\\s*=([^}]*)\\}\\}.*", "$1").trim();

                    System.out.println("id:\t" + id + "\tisalist:\t"
                            + isalist + "\tdisqualified:\t" + disqualified + "\tnotes:\t"
                            + notes + "\tMcoffsky:\t" + Mcoffsky + "\tСтефанко1982:\t"
                            + Стефанко1982 + "\tZheliba:\t" + Zheliba + "\tЗвірі:\t"
                            + Звірі + "\tDim_Grits:\t"
                            + Dim_Grits + "\thighlight:" + highlight);

                    CEESParsedUkwikiListedArticleVII thisone = new CEESParsedUkwikiListedArticleVII(
                            id, isalist, disqualified, notes, Mcoffsky,
                            Стефанко1982, Zheliba, Звірі, Dim_Grits, highlight);
                    parsed.put(id, thisone);
                }
            }

            String wrt = "{{CEES-MMXVII/Оцінки/Преамбула|}}\n";

            String table = "{{CEES-MMXVII/Оцінки/Шапка}}\n\n";

            String query
                    = "SELECT `article_id`,\n"
                    + "`article_title`,\n"
                    + "`author_name`,\n"
                    + "`author_isnoob`,\n"
                    + "`article_topic`,\n"
                    + "`article_wdentity`,\n"
                    + "`article_fromlist`,\n"
                    + "`article_authorbytessigma`,\n"
                    + "`article_authorbytesweeksigma`,\n"
                    + "`article_authoroldid`,\n"
                    + "`article_authorweekoldid`,\n"
                    + "`article_badge`\n"
                    + "FROM `2017`\n"
                    + "WHERE `wiki` = \"uk.wikipedia.org\" AND `article_fromweek` = " + week + "\n"
                    + "ORDER BY `article_authorbytesweeksigma` ASC";
//                + "ORDER BY `article_creationdate` ASC";
            System.out.println(query);

            ResultSet generalOutputtie = db.generalOutputtie(query);
            int counter = 0;
            String[] jurors = {
                "Mcoffsky",
                "Стефанко1982",
                "Zheliba",
                "Звірі"
            };

            int currentJuror = 0;

            while (generalOutputtie.next()) {

                String id = generalOutputtie.getInt(1) + "";
                String isalist = "";
                String disqualified = "";
                String notes = "";
                String Mcoffsky = "";
                String Стефанко1982 = "";
                String Zheliba = "";
                String Звірі = "";
                String Dim_Grits = "";
                String highlight = "";
                if (parsed.containsKey(id)) {
                    CEESParsedUkwikiListedArticleVII parserie = parsed.get(id);
                    isalist = parserie.isalist;
                    disqualified = parserie.disqualified;
                    notes = parserie.notes;
                    Mcoffsky = parserie.Mcoffsky;
                    Стефанко1982 = parserie.Стефанко1982;
                    Zheliba = parserie.Zheliba;
                    Звірі = parserie.Звірі;
                    Dim_Grits = parserie.Dim_Grits;
                    highlight = parserie.highlight;
                }

                table += "\n\n<!-- стаття з ідентифікатором " + generalOutputtie.getInt(1) + " -->";
                table += "\n{{CEES-MMXVII/Оцінки";
                table += "\n|лічильник         = " + ++counter;
                table += "\n|стаття            = " + generalOutputtie.getString(2);
                table += "\n|елемент_Вікіданих = " + generalOutputtie.getString(6);
                table += "\n|версія_тиждень    = " + generalOutputtie.getString(11);
                table += "\n|версія_конкурс    = " + generalOutputtie.getString(10);
                table += "\n|автор             = " + generalOutputtie.getString(3);
                table += "\n|додано_в_тижні    = " + generalOutputtie.getString(9);
                table += "\n|додано_остаточно  = " + generalOutputtie.getString(8);
                table += "\n|тема              = " + generalOutputtie.getString(5);
                table += "\n|зі_списків        = " + generalOutputtie.getString(7);
                table += "\n|бейджі            = " + generalOutputtie.getString(12);
                table += "\n|є_списком         = " + isalist;
                table += "\n|дискваліфіковано  = " + disqualified;
                table += "\n|зауваження        = " + notes;
                table += "\n|Mcoffsky          = " + Mcoffsky;
                table += "\n|Стефанко1982      = " + Стефанко1982;
                table += "\n|Zheliba           = " + Zheliba;
                table += "\n|Звірі             = " + Звірі;
                table += "\n|Dim_Grits         = " + Dim_Grits;
                table += "\n|підсвітка         = " + (highlight.equals("") ? jurors[currentJuror] : highlight);
                table += "\n}}";

//                try {
//                    CEESMMXVIUAMarkedArticle a = new CEESMMXVIUAMarkedArticle(
//                            generalOutputtie.getString(1),
//                            generalOutputtie.getString(2),
//                            generalOutputtie.getString(3),
//                            Integer.parseInt(generalOutputtie.getString(8)),
//                            generalOutputtie.getString(5),
//                            week,
//                            generalOutputtie.getString(12),
//                            "1".equals(generalOutputtie.getString(7)),
//                            "1".equals(isalist),
//                            !"".equals(disqualified),
//                            "".equals(Mcoffsky) ? -1 : Double.parseDouble(Mcoffsky.replaceAll(",", ".")),
//                            "".equals(Стефанко1982) ? -1 : Double.parseDouble(Стефанко1982.replaceAll(",", ".")),
//                            "".equals(Zheliba) ? -1 : Double.parseDouble(Zheliba.replaceAll(",", ".")),
//                            "".equals(Звірі) ? -1 : Double.parseDouble(Звірі.replaceAll(",", ".")),
//                            "".equals(Dim_Grits) ? -1 : Double.parseDouble(Dim_Grits.replaceAll(",", "."))
//                    );
//                    articles.add(a);
//                } catch (Exception ex) {
//
//                    System.out.println("Фігня з " + page + " зі статтею " + generalOutputtie.getString(2) + " а саме\n" + ex);
//                }
                if (counter % 2 == 0) {
                    currentJuror++;
                    currentJuror = currentJuror >= jurors.length ? 0 : currentJuror;
                }
            }

            table += "\n|}\n\n";

            wrt += table;

            wrt += "{{легенда|#ccffcc|статті, призначені для оцінювання певному члену журі (випадковим чином)}}\n";
            wrt += "{{легенда|LemonChiffon|статті, що ''поки що'' не відповідають "
                    + "[[:wmua:CEE Spring 2017/Умови конкурсу#Вимоги до статей|вимогам до статей]] і не оцінюються цього тижня}}\n";
            wrt += "{{легенда|mistyrose|дискваліфіковані статті, що не відповідають ключовим "
                    + "[[:wmua:CEE Spring 2017/Умови конкурсу#Вимоги до статей|вимогам до статей]] і не можуть бути прийняті до конкурсу}}";
            wrt += "\n\n";

            wrt += "== Результати ==\n\n\n";

            wrt += "== Примітки ==\n"
                    + "{{reflist}}\n\n\n";

            wrt += "[[Категорія:Вікіпроект:CEE Spring 2017]]";

            w.edit(page, wrt, "creating/updating");
            System.out.println(wrt);
        }

        // truncating our table
        /* mada mada
        
        String truncate = "TRUNCATE `2017-ukwiki-results`";
        Statement st = conn.createStatement();
        st.execute(truncate);
        for (CEESMMXVIUAMarkedArticle art : articles) {

            String query = "insert into `2017-ukwiki-results` "
                    + "(article_id, "
                    + "article_name, "
                    + "article_author, "
                    + "article_topic, "
                    + "article_week, "
                    + "article_mark, "
                    + "article_mark_ceiled_quality, "
                    + "article_mark_quality_two)"
                    + " values (?, ?, ?, ?, ?, ?, ?, ?)";

            PreparedStatement preparedStmt = conn.prepareStatement(query);
            System.out.println(art.articleID);
            preparedStmt.setString(1, art.articleID);
            System.out.println(art.articleName);
            preparedStmt.setString(2, art.articleName);
            System.out.println(art.author);
            preparedStmt.setString(3, art.author);
            System.out.println(art.topic);
            preparedStmt.setString(4, art.topic);
            System.out.println(art.week);
            preparedStmt.setInt(5, art.week);
            System.out.println(art.totalPoints);
            preparedStmt.setFloat(6, Float.parseFloat(art.totalPoints.toString()));
            System.out.println(art.totalPointsWithCeiledQuality);
            preparedStmt.setFloat(7, Float.parseFloat(art.totalPointsWithCeiledQuality.toString()));
            System.out.println(art.totalPointsWithQualityTwo);
            preparedStmt.setFloat(8, Float.parseFloat(art.totalPointsWithQualityTwo.toString()));
            // execute the prepared statement
            //preparedStmt.execute();
        }
         */
        db.stopDB();

    }

}
