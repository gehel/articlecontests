/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basebot.ceespring;

import basebot.ceespring.objects.CEESUser;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.Arrays;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import objects.Credentials;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import javax.security.auth.login.FailedLoginException;
import org.wikipedia.BaseBot;
import org.wikipedia.Wiki;

/**
 *
 * @author Base < base-w@yandex.ru >
 */
public class CEESpringWiki {

    /**
     * Name of this wiki in the format of its address e.g.
     * <tt>uk.wikipedia.org</tt>
     */
    String wikiName;
    String wikiWikidataName;
    String templateName;
    String languageCode;
    static Map<String, Map<String, String>> countries;
    static Map<String, Map<String, String>> topics;
    
    /**
     * Wikidata wiki
     */
    BaseBot d;
    /**
     * This instance's wiki
     */
    public BaseBot w;
    /**
     * Static field containing credintals as provided in execution args
     */
    static Credentials c;
    /**
     * An array of talk page names of wiki participating articles
     */
    String[] talkPages;
    /**
     * Boolean indicating that the wiki does not use the standard (talk page
     * template based) way of indicating article's affiliation to the contest
     */
    boolean templatePhobic;
    /**
     * Regular expression string used to match talk page template
     */
    String regex;
    
    Calendar contestStart;
    
    Calendar contestEnd;

    public CEESpringWiki(
            String wikiName,
            String tlname,
            String wdwiki,
            BaseBot d,
            Map<String, Map<String, String>> countries,
            Map<String, Map<String, String>> topics,
            Credentials c
    ){
        this.wikiName = wikiName;
        this.templateName = tlname;
        this.wikiWikidataName = wdwiki;
        this.d = d;
        CEESpringWiki.countries = countries;
        CEESpringWiki.topics = topics;
        CEESpringWiki.c = c;
    }
    
    
    /**
     *
     * @return @throws IOException
     */
    public String[] getTalkPages() throws IOException {
        return w.whatTranscludesHere(templateName, 1);
    }

    /**
     * Fetches texts for all pages connected to page talks given by batches of
     * limit given or the rest
     *
     * @param lim - limitation of number of articles texts to be fetched of per
     * batch
     * @return A map with keys for page titles and values for page texts
     * @throws Exception
     *
     * @todo Investigate why this method doesn't return <em>all</em> the page
     * texts
     */
    public Map<String, String> fetchPageTexts(int lim) throws Exception {
        String batchurlpart = "";
        Map<String, String> pageTexts = new HashMap<>();
        int j = 0;
        for (int i = 0; i < talkPages.length; i++) {
            String talk = talkPages[i];
            String article = talk.substring(talk.indexOf(":") + 1);
            batchurlpart += "".equals(batchurlpart) ? article : "|" + article;
            if (++j == lim || i == talkPages.length - 1 /*|| URLEncoder.encode(batchurlpart, "UTF-8").length() > 600*/) {
                pageTexts.putAll(w.batchPageTexts(batchurlpart));
                batchurlpart = "";
            }
        }
        j = 0;
        for (int i = 0; i < talkPages.length; i++) {
            String talk = talkPages[i];
            batchurlpart += "".equals(batchurlpart) ? talk : "|" + talk;
            if (++j == lim || i == talkPages.length - 1 || URLEncoder.encode(batchurlpart, "UTF-8").length() > 600) {
                pageTexts.putAll(w.batchPageTexts(batchurlpart));
                batchurlpart = "";
            }
        }
        return pageTexts;
    }

    int countArticleWords(String pageText) {
        return pageText.split("[\\p{Po}\\p{Z}\\p{S}\\p{C}]+").length;
    }

    static int getContestWeek(Calendar revisionTime, Calendar[][] contestWeeks) {
        int inWeek = 0;
        for (int i = 0; i <= 10; i++) {
            if (revisionTime.compareTo(contestWeeks[0][i]) >= 0 && revisionTime.compareTo(contestWeeks[1][i]) <= 0) {
                inWeek = i + 1;
                return inWeek;
            }
        }
        return inWeek;
    }

    void process() throws IOException, FailedLoginException, Exception {
        w = new BaseBot(wikiName);
        w.setUserAgent("BaseBot");
        w.login(c.wikilogin, c.wikipassword);
        w.setMarkBot(true);
        w.setMarkMinor(true);

        // true for wikis with the source of data other than the template
        // it makes no sense to try to parse talkpage for them
        boolean templatePhobic = false;
        Map<String, String> labsrepo = null;
        Map<String, String> templatePhobicArticles = new HashMap<>();
        talkPages = getTalkPages();

        boolean amIBot = w.getUser(c.wikilogin).isA("bot");
        int batchlim = amIBot ? 450 : 45;
//        ReplicationFetcher articlesDataFetcher = new ReplicationFetcher(args);
//        Map<String, CEESUser> articlesData = articlesDataFetcher.fetchArticlesData(3366 + 100 + thread, wdwiki, talkpages);
        Map<String, String> pageTexts = fetchPageTexts(batchlim);

        //TOPICS
        Set<String> topicNamesSet = topics.keySet();
        String[] topicNames = topicNamesSet.toArray(new String[topicNamesSet.size()]);
        Map<String, Set<String>> topicTalks = new HashMap<>();

        topicsiterator:
        for (String topicname : topicNames) {
            if (!topics.get(topicname).containsKey(wikiWikidataName)) {
                System.out.println("There are is " + topicname + " in " + wikiWikidataName);
                continue topicsiterator;
            }
            String topiccat = topics.get(topicname).get(wikiWikidataName);
            System.out.println(wikiWikidataName + "\t" + topiccat);
            String[] topicedtalks = w.getCategoryMembers(topiccat, 1);
            Set<String> talks = new HashSet<>();
            topicedtalksiterator:
            talks.addAll(Arrays.asList(topicedtalks));
            topicTalks.put(topicname, talks);
        } //END TOPICS

        //COUNTRIES
        Set<String> countryNamesSet = countries.keySet();
        String[] countryNames = countryNamesSet.toArray(new String[countryNamesSet.size()]);
        Map<String, Set<String>> countryTalks = new HashMap<>();

        countriesiterator:
        for (String countryname : countryNames) {
            if (!countries.get(countryname).containsKey(wikiWikidataName)) {
                System.out.println("There are no " + countryname + "-articles in " + wikiWikidataName);
                continue countriesiterator;
            }
            String countrycat = countries.get(countryname).get(wikiWikidataName);
            System.out.println(wikiWikidataName + "\t" + countrycat);
            String[] countriedtalks = w.getCategoryMembers(countrycat, 1);
            Set<String> talks = new HashSet<>();
            countriedtalksiterator:
            talks.addAll(Arrays.asList(countriedtalks));
            countryTalks.put(countryname, talks);
        } //END COUNTRIES

        Map<String, CEESUser> cashedUsers = new HashMap<>();
        Set<String> usersToFetch = new HashSet<>();

        talkpageiteration:
        for (String talkPage : talkPages) {
            String article = talkPage.substring(talkPage.indexOf(":") + 1);
            Map pageInfo = w.getPageInfo(article);
            long pageidentifier = (long) pageInfo.get("pageid");
            int pageid = Integer.parseInt(pageidentifier + "");//page id which we need
            int size = (int) pageInfo.get("size");// page size in bytes which we need
            Wiki.Revision[] pageHistory = w.getPageHistory(article);
            if (pageHistory.length == 0) {
                continue;
            }
            Wiki.Revision last = pageHistory[pageHistory.length - 1];
            Calendar timestamp = last.getTimestamp(); // page creation date which we need
            timestamp.setTimeZone(TimeZone.getTimeZone("UTC"));

            Calendar contestStart;
            Calendar contestEnd;
            Calendar[][] contestWeeks = null;
            Calendar week1_end = null;
            Calendar week2_start = null;
            Calendar week2_end = null;
            Calendar week3_start = null;
            Calendar week3_end = null;
            Calendar week4_start = null;
            Calendar week4_end = null;
            Calendar week5_start = null;
            Calendar week5_end = null;
            Calendar week6_start = null;
            Calendar week6_end = null;
            Calendar week7_start = null;
            Calendar week7_end = null;
            Calendar week8_start = null;
            Calendar week8_end = null;
            Calendar week9_start = null;
            Calendar week9_end = null;
            Calendar week10_start = null;
            Calendar week10_end = null;
            Calendar week11_start = null;
            Calendar week11_end = null;
            switch (wikiName) {
                //perfectly should be moved to some config
                //all the dates besides the very first should be calculated
                //basing on timezone. DST sucks.
                case "az.wikipedia.org":
                    contestStart = new GregorianCalendar(2016, 2, 31,
                            20, 00, 00);//Azerbaijan, UTC+4
                    contestEnd = new GregorianCalendar(2016, 4, 31,
                            19, 59, 59);//Azerbaijan, UTC+4
                    break;
                case "ba.wikipedia.org":
                    contestStart = new GregorianCalendar(2016, 2, 20,
                            19, 00, 00);//Bashkortostan, UTC+5
                    contestEnd = new GregorianCalendar(2016, 4, 31,
                            18, 59, 59);//Bashkortostan, UTC+5
                    break;
                case "be.wikipedia.org":
                    contestStart = new GregorianCalendar(2016, 2, 20,
                            21, 00, 00);//Belarus, UTC+3
                    contestEnd = new GregorianCalendar(2016, 4, 31,
                            20, 59, 59);//Belarus, UTC+3
                    break;
                case "be-tarask.wikipedia.org":
                    contestStart = new GregorianCalendar(2016, 2, 20,
                            21, 00, 00);//Belarus, UTC+3
                    contestEnd = new GregorianCalendar(2016, 4, 31,
                            20, 59, 59);//Belarus, UTC+3
                    break;
                case "bg.wikipedia.org":
                    contestStart = new GregorianCalendar(2016, 2, 20,
                            22, 00, 00);//Bulgaria, UTC+2
                    contestEnd = new GregorianCalendar(2016, 4, 31,
                            21, 59, 59);//Bulgaria, UTC+2
                    break;
                case "cs.wikipedia.org":
                    contestStart = new GregorianCalendar(2016, 2, 20,
                            23, 00, 00);//The Czech Republic, UTC+1
                    contestEnd = new GregorianCalendar(2016, 4, 31,
                            22, 59, 59);//The Czech Republic, UTC+1
                    break;
                case "de.wikipedia.org":
                    contestStart = new GregorianCalendar(2016, 2, 20,
                            22, 00, 00);//Austria, UTC+2
                    contestEnd = new GregorianCalendar(2016, 4, 31,
                            20, 59, 59);//Austria, UTC+3
                    break;
                case "eo.wikipedia.org":
                    contestStart = new GregorianCalendar(2016, 2, 20,
                            24, 00, 00);//Esperantujo, UTC
                    contestEnd = new GregorianCalendar(2016, 4, 31,
                            23, 59, 59);//Esperantujo, UTC
                    break;
                case "el.wikipedia.org":
                    contestStart = new GregorianCalendar(2016, 2, 20,
                            21, 00, 00);//Greece, UTC+3
                    contestEnd = new GregorianCalendar(2016, 4, 31,
                            20, 59, 59);//Greece, UTC+3
                    break;
                case "et.wikipedia.org":
                    contestStart = new GregorianCalendar(2016, 2, 20,
                            22, 00, 00);//Estonia, UTC+2
                    contestEnd = new GregorianCalendar(2016, 4, 31,
                            21, 59, 59);//Estonia, UTC+2
                    break;
                case "hu.wikipedia.org":
                    contestStart = new GregorianCalendar(2016, 2, 20,
                            22, 00, 00);//Hungary, UTC+2
                    contestEnd = new GregorianCalendar(2016, 4, 31,
                            20, 59, 59);//Hungary, UTC+3
                    break;
                case "ka.wikipedia.org":
                    contestStart = new GregorianCalendar(2016, 2, 20,
                            20, 00, 00);//Georgia, UTC+4
                    contestEnd = new GregorianCalendar(2016, 4, 31,
                            19, 59, 59);//Georgia, UTC+4
                    break;
                case "lt.wikipedia.org":
                    contestStart = new GregorianCalendar(2016, 2, 20,
                            22, 00, 00);//Lithuania, UTC+2
                    contestEnd = new GregorianCalendar(2016, 4, 31,
                            20, 59, 59);//Lithuania, UTC+3
                    break;
                case "lv.wikipedia.org":
                    contestStart = new GregorianCalendar(2016, 2, 20,
                            22, 00, 00);//Latvia, UTC+2
                    contestEnd = new GregorianCalendar(2016, 4, 31,
                            20, 59, 59);//Latvia, UTC+3
                    break;
                case "mk.wikipedia.org":
                    contestStart = new GregorianCalendar(2016, 2, 20,
                            23, 00, 00);//Macedonia, UTC+1
                    contestEnd = new GregorianCalendar(2016, 4, 31,
                            22, 59, 59);//Macedonia, UTC+1
                    break;
                case "pl.wikipedia.org":
                    contestStart = new GregorianCalendar(2016, 2, 20,
                            23, 00, 00);//Polonia, UTC+1
                    week1_end = new GregorianCalendar(2016, 2, 27,
                            21, 59, 59);
                    week2_start = new GregorianCalendar(2016, 2, 27,
                            22, 00, 00);
                    week2_end = new GregorianCalendar(2016, 3, 03,
                            21, 59, 59);
                    week3_start = new GregorianCalendar(2016, 3, 03,
                            22, 00, 00);
                    week3_end = new GregorianCalendar(2016, 3, 10,
                            21, 59, 59);
                    week4_start = new GregorianCalendar(2016, 3, 10,
                            22, 00, 00);
                    week4_end = new GregorianCalendar(2016, 3, 17,
                            21, 59, 59);
                    week5_start = new GregorianCalendar(2016, 3, 17,
                            22, 00, 00);
                    week5_end = new GregorianCalendar(2016, 3, 24,
                            21, 59, 59);
                    week6_start = new GregorianCalendar(2016, 3, 24,
                            22, 00, 00);
                    week6_end = new GregorianCalendar(2016, 4, 1,
                            21, 59, 59);
                    week7_start = new GregorianCalendar(2016, 4, 1,
                            22, 00, 00);
                    week7_end = new GregorianCalendar(2016, 4, 8,
                            21, 59, 59);
                    week8_start = new GregorianCalendar(2016, 4, 8,
                            22, 00, 00);
                    week8_end = new GregorianCalendar(2016, 4, 15,
                            21, 59, 59);
                    week9_start = new GregorianCalendar(2016, 4, 15,
                            22, 00, 00);
                    week9_end = new GregorianCalendar(2016, 4, 22,
                            21, 59, 59);
                    week10_start = new GregorianCalendar(2016, 4, 22,
                            22, 00, 00);
                    week10_end = new GregorianCalendar(2016, 4, 29,
                            21, 59, 59);
                    week11_start = new GregorianCalendar(2016, 4, 29,
                            22, 00, 00);
                    week11_end = new GregorianCalendar(2016, 4, 31,
                            21, 59, 59);
                    contestEnd = new GregorianCalendar(2016, 4, 31,
                            21, 59, 59);//Polonia, UTC+1
                    break;
                case "ro.wikipedia.org":
                    contestStart = new GregorianCalendar(2016, 2, 20,
                            22, 00, 00);//Romania, Moldova, UTC+2
                    contestEnd = new GregorianCalendar(2016, 4, 31,
                            21, 59, 59);//Romania, Moldova, UTC+2
                    break;
                case "ru.wikipedia.org":
                    contestStart = new GregorianCalendar(2016, 2, 20,
                            21, 00, 00);//Russia, UTC+3
                    contestEnd = new GregorianCalendar(2016, 4, 31,
                            20, 59, 59);//Russia, UTC+3
                    break;
                case "sk.wikipedia.org":
                    contestStart = new GregorianCalendar(2016, 2, 20,
                            23, 00, 00);//Slovakia, UTC+1
                    contestEnd = new GregorianCalendar(2016, 4, 31,
                            21, 59, 59);//Slovakia, UTC+2
                    break;
                case "sq.wikipedia.org":
                    contestStart = new GregorianCalendar(2016, 2, 20,
                            23, 00, 00);//Albania, Kosovo, UTC+1
                    contestEnd = new GregorianCalendar(2016, 4, 31,
                            22, 59, 59);//Albania, Kosovo, UTC+1
                    break;
                case "sr.wikipedia.org":
                    contestStart = new GregorianCalendar(2016, 2, 20,
                            23, 00, 00);//Serbia, Srpska, UTC+1
                    contestEnd = new GregorianCalendar(2016, 4, 31,
                            21, 59, 59);//Serbia, Srpska, UTC+2
                    break;
                case "tr.wikipedia.org":
                    contestStart = new GregorianCalendar(2016, 2, 20,
                            22, 00, 00);//Turkey, UTC+2
                    contestEnd = new GregorianCalendar(2016, 4, 31,
                            21, 59, 59);//Turkey, UTC+2
                    break;
                case "uk.wikipedia.org":
                    contestStart = new GregorianCalendar(2016, 2, 20,
                            22, 00, 00);//Ukraine, UTC+2
                    week1_end = new GregorianCalendar(2016, 2, 27,
                            20, 59, 59);
                    week2_start = new GregorianCalendar(2016, 2, 27,
                            21, 00, 00);
                    week2_end = new GregorianCalendar(2016, 3, 03,
                            20, 59, 59);
                    week3_start = new GregorianCalendar(2016, 3, 03,
                            21, 00, 00);
                    week3_end = new GregorianCalendar(2016, 3, 10,
                            20, 59, 59);
                    week4_start = new GregorianCalendar(2016, 3, 10,
                            21, 00, 00);
                    week4_end = new GregorianCalendar(2016, 3, 17,
                            20, 59, 59);
                    week5_start = new GregorianCalendar(2016, 3, 17,
                            21, 00, 00);
                    week5_end = new GregorianCalendar(2016, 3, 24,
                            20, 59, 59);
                    week6_start = new GregorianCalendar(2016, 3, 24,
                            21, 00, 00);
                    week6_end = new GregorianCalendar(2016, 4, 1,
                            20, 59, 59);
                    week7_start = new GregorianCalendar(2016, 4, 1,
                            21, 00, 00);
                    week7_end = new GregorianCalendar(2016, 4, 8,
                            20, 59, 59);
                    week8_start = new GregorianCalendar(2016, 4, 8,
                            21, 00, 00);
                    week8_end = new GregorianCalendar(2016, 4, 15,
                            20, 59, 59);
                    week9_start = new GregorianCalendar(2016, 4, 15,
                            21, 00, 00);
                    week9_end = new GregorianCalendar(2016, 4, 22,
                            20, 59, 59);
                    week10_start = new GregorianCalendar(2016, 4, 22,
                            21, 00, 00);
                    week10_end = new GregorianCalendar(2016, 4, 29,
                            20, 59, 59);
                    week11_start = new GregorianCalendar(2016, 4, 29,
                            21, 00, 00);
                    week11_end = new GregorianCalendar(2016, 4, 31,
                            20, 59, 59);
                    contestEnd = new GregorianCalendar(2016, 4, 31,
                            20, 59, 59);//Ukraine, UTC+3
                    break;
                default:
                    contestStart = new GregorianCalendar(2016, 2, 20,
                            00, 00, 00);//Globe, UTC-24;
                    contestEnd = new GregorianCalendar(2016, 5, 1,
                            23, 59, 59);//Globe, UTC+24;
            }
            contestStart.setTimeZone(TimeZone.getTimeZone("UTC"));
            int startOffset = timestamp.compareTo(contestStart);
            int endoff = timestamp.compareTo(contestEnd);

            if (endoff > 0) {//the page was created after the contest start -- nothing to do here
                continue talkpageiteration;
            }
            int inWeek = 0;

            String isImproved = "no";

            boolean wikiConsidersWeeks = "uk.wikipedia.org".equals(wikiName)
                    || "pl.wikipedia.org".equals(wikiName);
            if (wikiConsidersWeeks) {
                Calendar[] starts = {contestStart, week2_start, week3_start,
                    week4_start, week5_start, week6_start, week7_start,
                    week8_start, week9_start, week10_start, week11_start};
                Calendar[] ends = {week1_end, week2_end, week3_end, week4_end,
                    week5_end, week6_end, week7_end, week8_end, week9_end,
                    week10_end, week11_end};

                Calendar[][] weeksesies = {starts, ends};
                contestWeeks = weeksesies;
            }

            if (startOffset < 0) {
                isImproved = "yes";
            } else if (wikiConsidersWeeks) {
                inWeek = getContestWeek(timestamp, contestWeeks);
            }

            System.out.println("Startoff = " + startOffset);
//            int year = timestamp.get(Calendar.YEAR);
//            int month = timestamp.get(Calendar.MONTH) + 1;
//            int day = timestamp.get(Calendar.DAY_OF_MONTH);
//            int hour = timestamp.get(Calendar.HOUR_OF_DAY);
//            int minute = timestamp.get(Calendar.MINUTE);
//            int second = timestamp.get(Calendar.SECOND);
            boolean improverless = false;
            String improver = "Misterious CEE Springman";

            if (startOffset < 0 && !templatePhobic) {
//                System.out.println("Hey this one was created before the start! "
//                        + year + "-" + month + "-" + day + " " + hour + ":" + minute + ":" + second);
                improverless = true;

                boolean articleTextsContainArticle = pageTexts.containsKey(talkPage);
                String talkPageTemplate = "";
                if (articleTextsContainArticle) {
                    talkPageTemplate = pageTexts.get(talkPage);
                    System.out.println("We've just got a\t" + wikiName + "\t" + talkPage + "\ttext from the batch variable.");
                } else {
                    talkPageTemplate = w.getPageText(talkPage);
                }
                String regex = returnWikiRegexp(this);

                if (!"".equals(regex) && talkPageTemplate.matches(regex)) {
                    String improvercandidate = talkPageTemplate.replaceAll(regex, "$2").trim().replaceAll("_", " ");
                    if (!"".equals(improvercandidate)) {
                        improver = improvercandidate;
                        improverless = false;
                    }
                    System.out.println("Okay it's a " + wikiName + " improved article «" + article + "», the improver seem to be " + improver);
                } else {
                    System.out.println("No regex match for talk of\t" + article);
                }
            }

//            System.out.println("The time of first revision is: " + year + "-" + month + "-" + day + " " + hour + ":" + minute + ":" + second);
            String user = templatePhobic
                    ? labsrepo.get("Talk:" + article)
                    : (((startOffset < 0) && (!improverless))
                            ? improver
                            : last.getUser().trim()); // username which we need

            String gender = "";
            String userid = "";
            String caid = "";
            String isnoob = "";
            if (user.matches("\\A(25[0-5]|2[0-4]\\d|[0-1]?\\d?\\d)(\\.(25[0-5]|2[0-4]\\d|[0-1]?\\d?\\d)){3}\\z") || improverless) {
//                gender = "N/A";
//                userid = "N/A";
//                caid = "N/A";
//                isnoob = "N/A";
                CEESUser ceesUser = new CEESUser(user);
                System.out.println("Talk pager:\t" + ceesUser);
                cashedUsers.put(user, ceesUser);
            } else if (cashedUsers.containsKey(user)) {
//                    System.out.println("The cashing users thing actually works. At least for " + user);
//                    CEESUser cashedUser = cashedUsers.get(user);
//                    gender = cashedUser.gender;
//                    userid = cashedUser.identifier;
//                    caid = cashedUser.caid;
//                    isnoob = cashedUser.isNoob;

            } else {// API way should be uncommented into some method and used when replication lag is too high
//                    Wiki.User user1 = w.getUser(user);
//
//                    Map<String, Object> userInfo = user1.getUserInfo();
//                    if (userInfo.containsKey("gender")) {
//                        gender = ((Wiki.Gender) userInfo.get("gender")).toString(); // user gender which we need
//                    }
//                    if (userInfo.containsKey("userid")) {
//                        userid = (String) userInfo.get("userid"); // user id which we need
//                    }
//                    caid = w.CAId(user) + "";
//                    Calendar c = new GregorianCalendar(2016, 0, 21);
//                    c.set(2016, 0, 21);
//                    Wiki.Revision[] contribs = w.contribs(user, "", null, c, true);
//                    int noobind = contribs.length;
//                    isnoob = noobind <= 0 ? "yes" : "no"; //is noob? which we need
//
//                    cashedUsers.put(user, new CEESUser(userid, caid, user, gender, isnoob));
                usersToFetch.add(user);
            }
            boolean articleTextesContainArticle = pageTexts.containsKey(article);
            String pt = "";
            if (articleTextesContainArticle) {
                pt = pageTexts.get(article);
                System.out.println("We've just got a\t" + wikiName + "\t" + article + "\ttext from the batch variable.");
            } else {
                System.out.println("Fetching " + wikiName + "\t" + article + "\ttext from via API.");
                pt = w.getPageText(article);
            }
            int words = countArticleWords(pt);
            int bytessigma = 0;
            int weeksigma = 0;
            int weekoldid = 0;
            int authoroldid = 0;

            boolean hasvalidrevisions = false;
            Set<String> revhashes = new HashSet<>();

            revisioniteration:
            for (int j = pageHistory.length - 1; j >= 0; j--) {
                Wiki.Revision revision = pageHistory[j];
                String revuser = revision.getUser();
                int sizeDiff = revision.getSizeDiff();
                //System.out.println("We are listing revisions of the article " + article + " now we are on #" + j + " and the diff is " + sizeDiff);
                Calendar revisionTime = revision.getTimestamp();
                //System.out.println("revuser\t" + revuser + "user\t" + user);
                String hashCode = revision.getSha1();
                boolean alreadypresentrev = revhashes.contains(hashCode);
                revhashes.add(hashCode);

                if (revuser == null) {
                    continue revisioniteration;
                }
                if (revuser.equals(user)
                        && revisionTime.compareTo(contestStart) >= 0
                        && !alreadypresentrev
                        && (!(revisionTime.compareTo(contestEnd) > 0))) {
                    hasvalidrevisions = true;
                    bytessigma += sizeDiff;
                    authoroldid = (int) revision.getRevid();

                    //<editor-fold desc="crazywiki week selection">
                    if (wikiConsidersWeeks) {
                        if (startOffset < 0 && inWeek == 0) {
                            inWeek = getContestWeek(revisionTime, contestWeeks);
                        }
                        if (inWeek > 0) {
                            if (revisionTime.compareTo(contestWeeks[0][inWeek - 1]) >= 0 && revisionTime.compareTo(contestWeeks[1][inWeek - 1]) <= 0) {
                                weeksigma += sizeDiff;
                                weekoldid = 0;
                                weekoldid = (int) revision.getRevid();
                                //System.out.println(article + "\t" + j + "\t" + sizeDiff + "\t" + weekoldid);
                            }
                        }
                    }
                    //end of crazywiki if </editor-fold>
                }
            }

            if (!hasvalidrevisions) {
                System.out.println(article + "\thas no valid revisions");
                continue talkpageiteration;
            }

            final int weekolddamndfuckingshit = weekoldid;
            String wdentity;
            String humanity;
            String female;
            String badge;
            String[] WDEntityIsHuman = d.WDEntityIsHuman(article, wikiWikidataName);
            if (WDEntityIsHuman == null) {
                wdentity = "N/A";
                humanity = "N/A";
                female = "N/A";
                badge = "N/A";
            } else {
                wdentity = WDEntityIsHuman[0];
                humanity = WDEntityIsHuman[1];
                female = WDEntityIsHuman[2];
                badge = getHumanReadableBadgeMark(WDEntityIsHuman[3]);

            }

//            boolean isfromthelist = theLists.contains(wdentity);
//            System.out.println(article + "\t" + weekolddamndfuckingshit + "\tfrom lists:\t" + isfromthelist);
            String article_country = "";
            String article_topic = "";

            for (String topicname : topicNames) {
                if (topicTalks.containsKey(topicname)) {
                    if (topicTalks.get(topicname).contains(talkPage)) {
                        article_topic += ("".equals(article_topic) ? "" : ", ") + topicname;
                    }
                }
            }
            System.out.println(article + "\t" + article_topic);

            for (String countryname : countryNames) {
                if (countryTalks.containsKey(countryname)) {
                    if (countryTalks.get(countryname).contains(talkPage)) {
                        article_country += ("".equals(article_country) ? "" : ", ") + countryname;
                    }
                }
            }
            System.out.println(article + "\t" + article_country);

//            rows.add(new CEESTableLine(wikiName, pageid, article,
//                    /*userid, caid,*/ user, /*gender,
//        isnoob,*/ size, words,
//                    article_country, article_topic,
//                    humanity, female, new java.sql.Timestamp(timestamp.getTime().getTime()),
//                    wdentity, isfromthelist, isImproved, inWeek, bytessigma,
//                    weeksigma, authoroldid, weekolddamndfuckingshit, badge
//            ));
        } // end of going through talk pages

        String[] usersToFetchArray = usersToFetch.toArray(new String[usersToFetch.size()]);
        ReplicationFetcher cf = new ReplicationFetcher(c);
        //fetchUserData(String wikidb, String starttime, String noobtime, String... users)
//        Map<String, CEESUser> usersData = cf.fetchUsersData(
//                3366 + 1 + thread, wikiWikidataName,
//                "20160321000000",// hardcode
//                "20160121000000",// hardcode
//                usersToFetchArray
//        );
//        cashedUsers.putAll(usersData);
//
//        wikisusers.put(wikiName, cashedUsers);
//
//        maincounter++;
//        System.out.println(maincounter + "\t" + wikiName);
//        if (maincounter == maincounterlim) {
//            writeToDatabase();
//        }

    }

    /**
     * Returns hardcoded regex for given wiki
     *
     * @todo change to reading from some config
     * @param wikiname
     * @return
     */
    public static String returnWikiRegexp(CEESpringWiki wiki) {
        String regex = "";
        switch (wiki.wikiName) {//perfectly should be moved to some config
            case "az.wikipedia.org":
                    wiki.regex = "(?s).*\\{\\{\\s*[Vv]ikibahar[ _]2016\\s*\\|[^}]*(istifadəçi)\\s*=([^\\|}]+)[^}]*}}.*";
                    wiki.contestStart = new GregorianCalendar(2016, 2, 31,
                            20, 00, 00);//Azerbaijan, UTC+4
                    wiki.contestEnd = new GregorianCalendar(2016, 4, 31,
                            19, 59, 59);//Azerbaijan, UTC+4
                break;
            case "ba.wikipedia.org":
                regex = "(?s).*\\{\\{\\s*[Вв]ики-яҙ[ _]2016\\s*\\|[^}]*(ҡатнашыусы)\\s*=([^\\|}]+)[^}]*}}.*";
                break;
            case "be.wikipedia.org":
                regex = "(?s).*\\{\\{\\s*[Cc]EE[ _]Spring[ _]2016\\s*\\|[^}]*(удзельнік)\\s*=([^\\|}]+)[^}]*}}.*";
                break;
            case "be-tarask.wikipedia.org":
                regex = "(?s).*\\{\\{\\s*[Аа]ртыкул[ _]ВікіВясны-2016\\s*\\|[^}]*(удзельнік)\\s*=([^\\|}]+)[^}]*}}.*";
                //{{Артыкул ВікіВясны-2016 |удзельнік= ‎Lesnas ättling |тэма= геаграфія |тэма2= |тэма3= |краіна= Польшча |краіна2= |краіна3= }}
                break;
            case "bg.wikipedia.org":
                regex = "(?s).*\\{\\{\\s*[Cc]EE[ _]Spring[ _]2016\\s*\\|[^}]*(потребител)\\s*=([^\\|}]+)[^}]*}}.*";
                break;
            case "bs.wikipedia.org":
                regex = "(?s).*\\{\\{\\s*[Cc]EE[ _]Proljeće[ _]2016\\s*\\|[^}]*(korisnik)\\s*=([^\\|}]+)[^}]*}}.*";
                break;
            case "el.wikipedia.org":
                regex = "(?s).*\\{\\{\\s*[Cc]EE[ _]Spring[ _]2016\\s*\\|[^}]*(user)\\s*=([^\\|}]+)[^}]*}}.*";
                break;
            case "eo.wikipedia.org":
                regex = "(?s).*\\{\\{\\s*[Vv]iki-printempo[ _]2016\\s*\\|[^}]*(user)\\s*=([^\\|}]+)[^}]*}}.*";
                break;
            case "hu.wikipedia.org":
                regex = "(?s).*\\{\\{\\s*[Cc]EE[ _]Tavasz[ _]2016\\s*\\|[^}]*(szerkesztő)\\s*=([^\\|}]+)[^}]*}}.*";
                break;
            // case "hy.wikipedia.org":
            // regex = "(?s).*\\{\\{\\s*[Cc]EE[ _]Spring[ _]2016/հոդված\\s*\\|[^}]*(user)\\s*=([^\\|}]+)[^}]*}}.*";
            // no parameters at all
            // break;
            case "ka.wikipedia.org":
                regex = "(?s).*\\{\\{\\s*ვიკიგაზაფხული[ _]2016\\s*\\|[^}]*(მომხმარებელი)\\s*=([^\\|}]+)[^}]*}}.*";
                break;
            case "lt.wikipedia.org":
                regex = "(?s).*\\{\\{\\s*[Vv]ikipavasari[ _]2016\\s*\\|[^}]*(user)\\s*=([^\\|}]+)[^}]*}}.*";
                break;
            case "lv.wikipedia.org":
                regex = "(?s).*\\{\\{\\s*[Cc]EE[ _]Spring[ _]2016\\s*\\|[^}]*(user)\\s*=([^\\|}]+)[^}]*}}.*";
                break;
            case "mk.wikipedia.org":
                regex = "(?s).*\\{\\{\\s*[Сс]ИЕ[ _]Пролет[ _]2016\\s*\\|[^}]*(корисник)\\s*=([^\\|}]+)[^}]*}}.*";
                break;
            case "pl.wikipedia.org":
                regex = "(?s).*\\{\\{\\s*[Cc]EE[ _]Spring[ _]2016\\s*\\|[^}]*(autor)\\s*=([^\\|}]+)[^}]*}}.*";
                break;
            case "ro.wikipedia.org":
                regex = "(?s).*\\{\\{\\s*[Ww]ikimedia[ _]CEE[ _]Spring[ _]2016\\s*\\|[^}]*(utilizator|username)\\s*=([^\\|}]+)[^}]*}}.*";
                break;
            case "ru.wikipedia.org":
                regex = "(?s).*\\{\\{\\s*[Вв]ики-весна[ _]2016\\s*\\|[^}]*(участник)\\s*=([^\\|}]+)[^}]*}}.*";
                break;
            case "sah.wikipedia.org":
                regex = "(?s).*\\{\\{\\s*[Бб]иики-саас[ _]2016\\s*\\|[^}]*(кыттааччы)\\s*=([^\\|}]+)[^}]*}}.*";
                break;
            case "sk.wikipedia.org":
                regex = "(?s).*\\{\\{\\s*[Ww]ikiJar[ _]SVE[ _]2016\\s*\\|[^}]*(Redaktor)\\s*=([^\\|}]+)[^}]*}}.*";
                break;
            case "sq.wikipedia.org":
                regex = "(?s).*\\{\\{\\s*[Cc]EE[ _]Spring[ _]2016(?:/Kosova|)\\s*\\|[^}]*(user)\\s*=([^\\|}]+)[^}]*}}.*";
                break;
            case "sr.wikipedia.org":
                regex = "(?s).*\\{\\{\\s*[Cc]EE[ _]Spring[ _]2016\\s*\\|[^}]*(корисник)\\s*=([^\\|}]+)[^}]*}}.*";
                break;
            case "uk.wikipedia.org":
                //{{CEE Spring 2016 |користувач= Base |тема= історія |тема2= культура |тема3= |країна= Сербія |країна2= |країна3= }}
                regex = "(?s).*\\{\\{\\s*[Cc]EE[ _]Spring[ _]2016\\s*\\|[^}]*(користувач)\\s*=([^\\|}]+)[^}]*}}.*";
                break;
            default:
        }
        return regex;
    }

    public String getHumanReadableBadgeMark(String entityID) {
        String badgeString;
        switch (entityID) {
            case "Q17437796":
                badgeString = "FA";
                break;
            case "Q17437798":
                badgeString = "GA";
                break;
            case "Q17506997":
                badgeString = "FL";
                break;
            default:
                badgeString = "";
                break;
        }
        return badgeString;
    }

}
