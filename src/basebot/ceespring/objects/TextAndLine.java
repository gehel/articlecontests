/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basebot.ceespring.objects;

import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Base
 */
@XmlRootElement
public class TextAndLine {
    public String pageText;
    public CEESTableLine tableLine;
}
